#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>

#include <OSCMessage.h>
#include <Ethernet.h>
#include <EthernetUdp.h>
#include <SPI.h>    
#include <OSCMessage.h>
#include <WiFiUdp.h>

WiFiUDP Udp;

//the Arduino's IP
IPAddress ip(128, 32, 122, 252);
//destination IP
IPAddress outIp(192, 168, 1, 69);
const unsigned int outPort = 8000;

 byte mac[] = {  
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED }; // you can find this written on the board of some Arduino Ethernets or shields


char ssid[] = "kandeimaging";
char pass[] = "toffee15";
#define relay D5
#define mic D4
#define analogMic A0

void setup() {

  pinMode(LED_BUILTIN, OUTPUT);

  Serial.begin(115200);
  while(!Serial){
    Serial.print(". ");
    delay(100);
    };
  Serial.println("Started");

  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(100);                       // wait for a second
    digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
    delay(100); 
    Serial.println(".");
  }
  Serial.println("connected");
  Udp.begin(8000);
  
}

void loop() {

  //the message wants an OSC address as first argument
  //char playStop[] = "/action/40044";
  
  OSCMessage msg("/action/40044");

  //msg.add((int32_t)analogRead(0));
    
  Udp.beginPacket(outIp, outPort);
  
  msg.send(Udp); // send the bytes to the SLIP stream

  Udp.endPacket(); // mark the end of the OSC Packet

  Serial.print("message ");
  Serial.println("");    
  
  msg.empty(); // free space occupied by message
  
  for (int i = 0; i < 5; i++){
    digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(20);                       // wait for a second
    digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
    delay(20); 
  }

  delay(800);

}

